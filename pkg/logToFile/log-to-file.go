package logToFile

import (
	"log"
	"os"
)

// LogToFile redirects log output to the given filename and then logs the given message to that file
func LogToFile(filename, message string) error {
	//open the file we'll be writing to
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return err
	}
	//defer closing it until the function terminates
	defer f.Close()

	//set log output to write to the file
	log.SetOutput(f)

	//log message to file
	log.Println(message)

	return nil
}
