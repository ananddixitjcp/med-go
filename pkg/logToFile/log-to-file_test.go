package logToFile

import (
	"io/ioutil"
	//"log"
	"os"
	"strings"
	"testing"
)

// TestLog tests the functionality of writing logs to files by calling LogToFile and then reading the given filename to ensure the message was added correctly
func TestLog(t *testing.T) {
	t.Log("##### testing logging messages to local file... #####")

	t.Log("logging 'this should appear in logfile'")
	err := LogToFile("testlog.txt", "this should appear in logfile")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}

	data, err := ioutil.ReadFile("testlog.txt")
	if err != nil {
		t.Error("error reading file:", err)
		return
	}

	contents := string(data)

	t.Log("contents of file are: " + contents)

	if !strings.Contains(contents, "this should appear in logfile") {
		t.Error("failed to find expected string in file")
		return
	}

	os.Remove("testlog.txt")

	t.Log("logging 'test 1'")
	err = LogToFile("testlog.txt", "test 1")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}
	t.Log("logging 'test 2'")
	err = LogToFile("testlog.txt", "test 2")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}
	t.Log("logging 'test 3'")
	err = LogToFile("testlog.txt", "test 3")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}
	t.Log("logging 'test 4'")
	err = LogToFile("testlog.txt", "test 4")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}

	data, err = ioutil.ReadFile("testlog.txt")
	if err != nil {
		t.Error("error reading file:", err)
		return
	}
	contents = string(data)

	t.Log("contents of file are: " + contents)

	if !strings.Contains(contents, "test 1") {
		t.Error("failed to find expected string in file")
		return
	}
	if !strings.Contains(contents, "test 2") {
		t.Error("failed to find expected string in file")
		return
	}
	if !strings.Contains(contents, "test 3") {
		t.Error("failed to find expected string in file")
		return
	}
	if !strings.Contains(contents, "test 4") {
		t.Error("failed to find expected string in file")
		return
	}

	os.Remove("testlog.txt")

	t.Log("Success!")

}
