package kafka

import (
	"github.com/optiopay/kafka" // Go client library for Apache Kafka
	"github.com/optiopay/kafka/proto"

	"log"
	"time"
)

// Store the partitiion we are using as a constant if it will be constant for all messages (if not, we could add it as a parameter to ProduceMessage)
const partition = 0

// Store array of addresses to connect to
var addrs = []string{"localhost:9092"}

// Init opens a connection and return the client
func Init(clientID string) (kafka.Client, error) {
	conf := kafka.NewBrokerConf(clientID)
	conf.AllowTopicCreation = true
	//connect to a specific client/broker (Client is the interface implemented by Broker.)
	broker, err := kafka.Dial(addrs, conf)
	if err != nil {
		return nil, err
	}

	return broker, nil
}

// ProduceMessage produces a message to broker with topic topic and body message.
func ProduceMessage(broker kafka.Client, topic, message string) error {
	producer := broker.Producer(kafka.NewProducerConf())

	msg := &proto.Message{Value: []byte(message)}

	_, err := producer.Produce(topic, partition, msg)
	if err != nil {
		return err
	}
	return nil
}

// Consume is a wrapper around consumeMessages to spin off a new goroutine for consuming and then return
func Consume(broker kafka.Client, topic string, ch chan string) {
	go consumeMessages(broker, topic, ch)
	//give it a little time to start up before proceeding so we don't risk missing first message
	//in testing startup never took more that 1 millisecond, so 5 ms should be ample time
	time.Sleep(5 * time.Millisecond)
}

//consume all messages and send them down channel
func consumeMessages(broker kafka.Client, topic string, ch chan string) {
	//close channel to signal that we're done at end
	defer close(ch)

	//create consumer
	conf := kafka.NewConsumerConf(topic, partition)
	//only process new messages
	conf.StartOffset = kafka.StartOffsetNewest
	//don't block forever on Consume() before returning ErrNoData
	conf.RetryLimit = 10
	consumer, err := broker.Consumer(conf)
	if err != nil {
		log.Panic(err)
	}

	//loop consuming and printing messages
	for {
		msg, err := consumer.Consume()
		if err != nil {
			if err != kafka.ErrNoData {
				log.Panic(err)
			}
			break
		}
		ch <- "message on topic " + msg.Topic + ": " + string(msg.Value)
	}

}
