package kafka

import (
	"testing"
)

//TesKafka tests the connection to kafka server
func TestKafka(t *testing.T) {
	t.Log("##### testing kafka server... #####")

	broker, err := Init("test-client")
	if err != nil {
		t.Error("Error initializing kafka", err)
		return
	}
	defer broker.Close()

	//test with a single message
	ch := make(chan string, 10)
	Consume(broker, "test-topic", ch)
	t.Log("sending 'test message 1' to 'test-topic'")

	err = ProduceMessage(broker, "test-topic", "test message 1")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}
	for s := range ch {
		t.Log("received " + s)
		if "message on topic test-topic: test message 1" != s {
			t.Error("strings do not match!")
			return
		}
	}

	//test with a series of messages to ensure they all come through
	ch = make(chan string, 10)
	Consume(broker, "test-topic2", ch)

	t.Log("sending 'test message 2a' to 'test-topic2'")
	err = ProduceMessage(broker, "test-topic2", "test message 2a")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}

	t.Log("sending 'test message 2b' to 'test-topic2'")
	err = ProduceMessage(broker, "test-topic2", "test message 2b")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}

	t.Log("sending 'test message 2c' to 'test-topic2'")
	err = ProduceMessage(broker, "test-topic2", "test message 2c")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}

	t.Log("sending 'test message 2d' to 'test-topic2'")
	err = ProduceMessage(broker, "test-topic2", "test message 2d")
	if err != nil {
		t.Error("Error producing message", err)
		return
	}

	var a, b, c, d bool
	for s := range ch {
		t.Log("received " + s)
		if s == "message on topic test-topic2: test message 2a" {
			if a {
				t.Error("already received test message 2a")
				return
			}
			a = true
		} else if s == "message on topic test-topic2: test message 2b" {
			if b {
				t.Error("already received test message 2b")
				return
			}
			b = true
		} else if s == "message on topic test-topic2: test message 2c" {
			if c {
				t.Error("already received test message 2c")
				return
			}
			c = true
		} else if s == "message on topic test-topic2: test message 2d" {
			if d {
				t.Error("already received test message 2d")
				return
			}
			d = true
		} else {
			t.Error("received too many messages")
			return
		}
	}
	if !a {
		t.Error("didn't receive test message 2a")
		return
	}
	if !b {
		t.Error("didn't receive test message 2b")
		return
	}
	if !c {
		t.Error("didn't receive test message 2c")
		return
	}
	if !d {
		t.Error("didn't receive test message 2d")
		return
	}

	t.Log("Success!")
}
