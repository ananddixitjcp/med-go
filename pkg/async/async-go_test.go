package async

import (
	"fmt"
	"testing"
)

// TestAsync demonstrates how we can mimic JavaScript's async functionality using goroutines
func TestAsync(t *testing.T) {
	fmt.Println("##### demonstrating async functionality of goroutines: #####\n")
	Run()
}
