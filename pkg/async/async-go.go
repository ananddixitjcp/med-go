package async

import (
	"fmt"
	"sync"
	"time"
)

// Run runs and times the synchronous and then asynchronous sleep of 3 kids for 2 seconds each
func Run() {
	fmt.Println("3 kids sleeping synchronously for 2 seconds each...")
	start := time.Now()
	sleepInOrder()
	elapsed := time.Since(start)
	fmt.Printf("That took %s!\n", elapsed)

	time.Sleep(1 * time.Second)

	fmt.Println("Now 3 kids sleeping for 2 seconds each asynchronously using goroutines...")
	start = time.Now()
	sleepAsync()
	elapsed = time.Since(start)
	fmt.Printf("That only took %s!\n", elapsed)

}

// The sleepInOrder function performs the synchronous sleeping of 3 kids for 2 seconds each
func sleepInOrder() {
	fmt.Println("#1 falling asleep for 2 seconds...")
	time.Sleep(2 * time.Second)
	fmt.Println("#1 awake!")
	fmt.Println("#2 falling asleep for 2 seconds...")
	time.Sleep(2 * time.Second)
	fmt.Println("#2 awake!")
	fmt.Println("#3 falling asleep for 2 seconds...")
	time.Sleep(2 * time.Second)
	fmt.Println("#3 awake!")
}

// The sleepAsync function performs the asynchronous sleeping of 3 kids for 2 seconds each
func sleepAsync() {
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		fmt.Println("#1 falling asleep for 2 seconds...")
		time.Sleep(2 * time.Second)
		fmt.Println("#1 awake!")
		wg.Done()
	}()
	go func() {
		fmt.Println("#2 falling asleep for 2 seconds...")
		time.Sleep(2 * time.Second)
		fmt.Println("#2 awake!")
		wg.Done()
	}()
	go func() {
		fmt.Println("#3 falling asleep for 2 seconds...")
		time.Sleep(2 * time.Second)
		fmt.Println("#3 awake!")
		wg.Done()
	}()
	wg.Wait()
}
