package params

import (
	"errors"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

//some sample params that we may have in the URL
type requestParams struct {
	pid            string
	did            string
	bid            string
	ip             string
	ua             string
	lat            float64
	long           float64
	country        string
	state          string
	city           string
	dma            string
	zip            int
	device_id      string
	limit_ad_track bool
	ad_format      string
	lang           string
	st             string
	os_type        string
	portrait       bool
	device_make    string
	device_model   string
	app_name       string
	app_ver        string
	bundle_id      string
	screen_width   int
	screen_height  int
	sdk_version    string
	access_token   string
	app_domain     string
	adv_type       string
	res_type       string
	ad_type        string
	app_privacy    string
	cb             string

	//add more as needed
}

func (rp requestParams) String() string {
	return "'pid': " + rp.pid + ", 'did': " + rp.did + ", 'bid': " + rp.bid + ", 'ip': " + rp.ip + ", 'ua': " + rp.ua + ", 'lat': " +
		strconv.FormatFloat(rp.lat, 'f', -1, 64) + ", 'long': " + strconv.FormatFloat(rp.long, 'f', -1, 64) + ", 'country': " + rp.country + ", 'state': " + rp.state +
		", 'city': " + rp.city + ",'dma': " + rp.dma + ", 'zip': " + strconv.FormatInt(int64(rp.zip), 10) + ", 'device_id': " + rp.device_id +
		", 'limit_ad_track': " + strconv.FormatBool(rp.limit_ad_track) + ", 'ad_format': " + rp.ad_format + ", 'lang': " + rp.lang +
		", 'st': " + rp.st + ", 'os_type': " + rp.os_type + ", 'portrait': " + strconv.FormatBool(rp.portrait) + ", 'device_make': " + rp.device_make +
		", 'device_model': " + rp.device_model + ", 'app_name': " + rp.app_name + ", 'app_ver': " + rp.app_ver + ", 'bundle_id': " + rp.bundle_id +
		", 'screen_width': " + strconv.FormatInt(int64(rp.screen_width), 10) + ", 'screen_height': " + strconv.FormatInt(int64(rp.screen_height), 10) +
		", 'sdk_version': " + rp.sdk_version + ", 'access_token': " + rp.access_token + ", 'app_domain': " + rp.app_domain + ", 'adv_type': " + rp.adv_type +
		", 'res_type': " + rp.res_type + ", 'did': " + rp.ad_type + ", 'app_privacy': " + rp.app_privacy + ", 'cb': " + rp.cb
}

// startServer starts a server running if we want to actually test it in a browser
func startServer() {
	//add handler for URL that will contain parameters
	http.HandleFunc("/sample", ParamHandler)
	//listen on localhost:3000
	http.ListenAndServe(":3000", nil)
}

// ParamHandler processes query params from url of form '/sample?xxxxx'
//can test with: http://localhost:3000/sample?pid=PG-2-10143-00000450&ip=24.115.255.255&ua=Mozilla%2F5.0%20%28iPad%3B%20CPU%20OS%208_4%20like%20Mac%20OS%20X%29%20AppleWebKit%2F600.1.4%20%28KHTML%2C%20like%20Gecko%29%20Mobile%2F12H143&device_id=07c20fdf-d97c-4c36-96e1-3a7951167dfb&limit_ad_track=true&ad_format=ALL&lang=en&st=mobile_app&os_type=IOS&portrait=true&device_make=Apple&device_model=iPad3,1&app_name=PGMSDKSample&app_ver=1.0&bundle_id=com.47Billion.monetizeSDKTest&screen_width=768&screen_height=1024&sdk_version=1.2.2&access_token=33323f57-e882-4c1e-a221-f854017ac932&app_domain=mygame.pg.com&adv_type=VIDEO&res_type=VAST&ad_type=VPIMP4&app_privacy=1&cb=234430&did=test&bid=test&lat=37.3830553&long=-122.0056697&country=us&state=ca&city=sunnyvale&dma=san%20francisco-oak-san%20jose&zip=94085
func ParamHandler(w http.ResponseWriter, r *http.Request) {
	u := r.URL

	mapParams := u.Query()

	//respond with a 404 if a required param is missing
	paramObj, err := checkParams(mapParams)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)

	io.WriteString(w, paramObj.String())

	//fmt.Println(paramObj, err)
}

//checkParams checks to make sure that all parameters are present and add them to a requestParams struct
//TODO: add validation for more params
//TODO: if a parameter is not required, remove error and assign default value if not present
func checkParams(mapParams map[string][]string) (requestParams, error) {
	var paramObj requestParams

	//check pid
	if val, ok := mapParams["pid"]; !ok {
		return paramObj, errors.New("missing param: pid")
	} else {
		paramObj.pid = strings.Join(val, " ")
	}

	//check did
	if val, ok := mapParams["did"]; !ok {
		return paramObj, errors.New("missing param: did")
	} else {
		paramObj.did = strings.Join(val, " ")
	}

	//check bid
	if val, ok := mapParams["bid"]; !ok {
		return paramObj, errors.New("missing param: bid")
	} else {
		paramObj.bid = strings.Join(val, " ")
	}

	//check ip
	if val, ok := mapParams["ip"]; !ok {
		return paramObj, errors.New("missing param: ip")
	} else {
		paramObj.ip = strings.Join(val, " ")

		pattern := `\A(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\z`
		matched, err := regexp.MatchString(pattern, paramObj.ip)
		if !matched || err != nil {
			return paramObj, errors.New("invalid param: ip")
		}
	}

	//check ua
	if val, ok := mapParams["ua"]; !ok {
		return paramObj, errors.New("missing param: ua")
	} else {
		paramObj.ua = strings.Join(val, " ")
	}

	//check lat
	if val, ok := mapParams["lat"]; !ok {
		return paramObj, errors.New("missing param: lat")
	} else {
		lat, err := strconv.ParseFloat(strings.Join(val, " "), 64)
		if err != nil || lat > 90.0 || lat < -90.0 {
			return paramObj, errors.New("invalid param: lat")
		}
		paramObj.lat = lat
	}

	//check long
	if val, ok := mapParams["long"]; !ok {
		return paramObj, errors.New("missing param: long")
	} else {
		long, err := strconv.ParseFloat(strings.Join(val, " "), 64)
		if err != nil || long > 180.0 || long < -180.0 {
			return paramObj, errors.New("invalid param: long")
		}
		paramObj.long = long
	}

	//check country
	if val, ok := mapParams["country"]; !ok {
		return paramObj, errors.New("missing param: country")
	} else {
		paramObj.country = strings.Join(val, " ")
		countries := [249]string{"AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BQ", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CW", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SX", "SK", "SI", "SB", "SO", "ZA", "GS", "SS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW"}
		found := false
		for _, v := range countries {
			if strings.ToUpper(paramObj.country) == v {
				found = true
				break
			}
		}
		if !found {
			return paramObj, errors.New("invalid param: country")
		}
	}

	//check city
	if val, ok := mapParams["city"]; !ok {
		return paramObj, errors.New("missing param: city")
	} else {
		paramObj.city = strings.Join(val, " ")
	}

	//check state (if country is us)
	if strings.ToLower(paramObj.country) == "us" {
		if val, ok := mapParams["state"]; !ok {
			return paramObj, errors.New("missing param: state")
		} else {
			paramObj.state = strings.Join(val, " ")
			states := [50]string{"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"}
			found := false
			for _, v := range states {
				if strings.ToUpper(paramObj.state) == v {
					found = true
					break
				}
			}
			if !found {
				return paramObj, errors.New("invalid param: state")
			}
		}
	}

	//check dma (if country is us)
	if strings.ToLower(paramObj.country) == "us" {
		if val, ok := mapParams["dma"]; !ok {
			return paramObj, errors.New("missing param: dma")
		} else {
			paramObj.dma = strings.Join(val, " ")
			dmas := [210]string{"New York", "Los Angeles", "Chicago", "Philadelphia", "Dallas-Ft. Worth", "San Francisco-Oak-San Jose", "Washington, DC (Hagrstwn)", "Houston", "Boston (Manchester)", "Atlanta", "Tampa-St. Pete (Sarasota)", "Phoenix (Prescott)", "Detroit", "Seattle-Tacoma", "Minneapolis-St. Paul", "Miami-Ft. Lauderdale", "Denver", "Orlando-Daytona Bch-Melbrn", "Cleveland-Akron (Canton)", "Sacramnto-Stkton-Modesto", "St. Louis", "Charlotte", "Pittsburgh", "Raleigh-Durham (Fayetvlle)", "Portland, OR", "Baltimore", "Indianapolis", "San Diego", "Nashville", "Hartford & New Haven", "San Antonio", "Columbus, OH", "Kansas City", "Salt Lake City", "Milwaukee", "Cincinnati", "Greenvll-Spart-Ashevll-And", "West Palm Beach-Ft. Pierce", "Austin", "Las Vegas", "Oklahoma City", "Norfolk-Portsmth-Newpt Nws", "Harrisburg-Lncstr-Leb-York", "Grand Rapids-Kalmzoo-B.Crk", "Birmingham (Ann and Tusc)", "Greensboro-H.Point-W.Salem", "Jacksonville", "Albuquerque-Santa Fe", "Louisville", "New Orleans", "Memphis", "Providence-New Bedford", "Buffalo", "Fresno-Visalia", "Richmond-Petersburg", "Wilkes Barre-Scranton-Hztn", "Little Rock-Pine Bluff", "Tulsa", "Albany-Schenectady-Troy", "Mobile-Pensacola (Ft Walt)", "Ft. Myers-Naples", "Knoxville", "Lexington", "Dayton", "Honolulu", "Wichita-Hutchinson Plus", "Roanoke-Lynchburg", "Green Bay-Appleton", "Des Moines-Ames", "Charleston-Huntington", "Tucson (Sierra Vista)", "Flint-Saginaw-Bay City", "Spokane", "Omaha", "Springfield, MO", "Rochester, NY", "Columbia, SC", "Toledo", "Huntsville-Decatur (Flor)", "Madison", "Portland-Auburn", "Shreveport", "Paducah-Cape Girard-Harsbg", "Harlingen-Wslco-Brnsvl-McA", "Syracuse", "Champaign&Sprngfld-Decatur", "Waco-Temple-Bryan", "Colorado Springs-Pueblo", "Chattanooga", "Cedar Rapids-Wtrlo-IWC&Dub", "Savannah", "El Paso (Las Cruces)", "Baton Rouge", "Charleston, SC", "Jackson, MS", "South Bend-Elkhart", "Burlington-Plattsburgh", "Tri-Cities, TN-VA", "Ft. Smith-Fay-Sprngdl-Rgrs", "Greenville-N.Bern-Washngtn", "Davenport-R.Island-Moline", "Myrtle Beach-Florence", "Evansville", "Johnstown-Altoona-St Colge", "Lincoln & Hastings-Krny", "Boise", "Tallahassee-Thomasville", "Tyler-Longview(Lfkn&Ncgd)", "Sioux Falls(Mitchell)", "Ft. Wayne", "Augusta-Aiken", "Reno", "Lansing", "Springfield-Holyoke", "Youngstown", "Fargo-Valley City", "Eugene", "Peoria-Bloomington", "Traverse City-Cadillac", "Lafayette, LA", "Macon", "Yakima-Pasco-Rchlnd-Knnwck", "Montgomery-Selma", "SantaBarbra-SanMar-SanLuOb", "Monterey-Salinas", "Bakersfield", "Columbus, GA (Opelika, AL)", "Corpus Christi", "La Crosse-Eau Claire", "Wilmington", "Amarillo", "Chico-Redding", "Columbus-Tupelo-W Pnt-Hstn", "Wausau-Rhinelander", "Topeka", "Columbia-Jefferson City", "Monroe-El Dorado", "Rockford", "Medford-Klamath Falls", "Minot-Bsmrck-Dcknsn(Wlstn)", "Beaumont-Port Arthur", "Duluth-Superior", "Odessa-Midland", "Salisbury", "Lubbock", "Palm Springs", "Anchorage", "Wichita Falls & Lawton", "Sioux City", "Erie", "Joplin-Pittsburg", "Albany, GA", "Rochestr-Mason City-Austin", "Panama City", "Terre Haute", "Bangor", "Biloxi-Gulfport", "Wheeling-Steubenville", "Bluefield-Beckley-Oak Hill", "Binghamton", "Gainesville", "Sherman-Ada", "Idaho Fals-Pocatllo(Jcksn)", "Missoula", "Abilene-Sweetwater", "Billings", "Yuma-El Centro", "Hattiesburg-Laurel", "Clarksburg-Weston", "Quincy-Hannibal-Keokuk", "Utica", "Rapid City", "Dothan", "Lake Charles", "Elmira (Corning)", "Jackson, TN", "Harrisonburg", "Watertown", "Alexandria, LA", "Marquette", "Bowling Green", "Jonesboro", "Charlottesville", "Laredo", "Butte-Bozeman", "Grand Junction-Montrose", "Lafayette, IN", "Bend, OR", "Lima", "Meridian", "Twin Falls", "Great Falls", "Greenwood-Greenville", "Parkersburg", "Eureka", "San Angelo", "Casper-Riverton", "Cheyenne-Scottsbluff", "Mankato", "Ottumwa-Kirksville", "St. Joseph", "Fairbanks", "Victoria", "Zanesville", "Helena", "Presque Isle", "Juneau", "Alpena", "North Platte", "Glendive"}
			found := false
			for _, v := range dmas {
				if strings.ToLower(paramObj.dma) == strings.ToLower(v) {
					found = true
					break
				}
			}
			if !found {
				return paramObj, errors.New("invalid param: dma")
			}
		}
	}

	//check zip
	if val, ok := mapParams["zip"]; !ok {
		return paramObj, errors.New("missing param: zip")
	} else {
		zip, err := strconv.ParseInt(strings.Join(val, " "), 10, 0)
		if err != nil {
			return paramObj, errors.New("invalid param: zip")
		}
		paramObj.zip = int(zip)
	}

	//check device_id
	if val, ok := mapParams["device_id"]; !ok {
		return paramObj, errors.New("missing param: device_id")
	} else {
		paramObj.device_id = strings.Join(val, " ")
	}

	//check limit_ad_track
	if val, ok := mapParams["limit_ad_track"]; !ok {
		return paramObj, errors.New("missing param: limit_ad_track")
	} else {
		limit_ad_track, err := strconv.ParseBool(strings.Join(val, " "))
		if err != nil {
			return paramObj, errors.New("invalid param: limit_ad_track")
		}
		paramObj.limit_ad_track = limit_ad_track
	}

	//check ad_format
	if val, ok := mapParams["ad_format"]; !ok {
		return paramObj, errors.New("missing param: ad_format")
	} else {
		paramObj.ad_format = strings.Join(val, " ")
	}

	//check lang
	if val, ok := mapParams["lang"]; !ok {
		return paramObj, errors.New("missing param: lang")
	} else {
		paramObj.lang = strings.Join(val, " ")
		langs := [191]string{"aa", "ab", "af", "ak", "sq", "am", "ar", "an", "hy", "as", "av", "ae", "ay", "az", "ba", "bm", "be", "bn", "bh", "bi", "bo", "bs", "br", "bg", "my", "ca", "cs", "ch", "ce", "zh", "cu", "cv", "kw", "co", "cr", "cy", "cs", "da", "de", "dv", "nl", "dz", "el", "en", "eo", "et", "eu", "ee", "fo", "fa", "fj", "fi", "fr", "fy", "ff", "ka", "gd", "ga", "gl", "gv", "gn", "gu", "ht", "ha", "he", "hz", "hi", "ho", "hr", "hu", "ig", "is", "io", "ii", "iu", "ie", "ia", "id", "ik", "it", "jv", "ja", "kl", "kn", "ks", "kr", "kk", "km", "ki", "rw", "ky", "kv", "kg", "ko", "kj", "ku", "lo", "la", "lv", "li", "ln", "lt", "lb", "lu", "lg", "mk", "mh", "ml", "mi", "mr", "ms", "mg", "mt", "mn", "my", "na", "nv", "nr", "nd", "ng", "ne", "nl", "nn", "nb", "no", "ny", "oc", "oj", "or", "om", "os", "pa", "fa", "pi", "pl", "pt", "ps", "qu", "rm", "ro", "rn", "ru", "sg", "sa", "si", "sk", "sl", "se", "sm", "sn", "sd", "so", "st", "es", "sq", "sc", "sr", "ss", "su", "sw", "sv", "ty", "ta", "tt", "te", "tg", "tl", "th", "ti", "to", "tn", "ts", "tk", "tr", "tw", "ug", "uk", "ur", "uz", "ve", "vi", "vo", "cy", "wa", "wo", "xh", "yi", "yo", "za", "zh", "zu"}
		found := false
		for _, v := range langs {
			if strings.ToLower(paramObj.lang) == v {
				found = true
				break
			}
		}
		if !found {
			return paramObj, errors.New("invalid param: lang")
		}
	}

	//check st
	if val, ok := mapParams["st"]; !ok {
		return paramObj, errors.New("missing param: st")
	} else {
		paramObj.st = strings.Join(val, " ")
	}

	//check os_type
	if val, ok := mapParams["os_type"]; !ok {
		return paramObj, errors.New("missing param: os_type")
	} else {
		paramObj.os_type = strings.Join(val, " ")
	}

	//check portrait
	if val, ok := mapParams["portrait"]; !ok {
		return paramObj, errors.New("missing param: portrait")
	} else {
		portrait, err := strconv.ParseBool(strings.Join(val, " "))
		if err != nil {
			return paramObj, errors.New("invalid param: portrait")
		}
		paramObj.portrait = portrait
	}

	//check device_make
	if val, ok := mapParams["device_make"]; !ok {
		return paramObj, errors.New("missing param: device_make")
	} else {
		paramObj.device_make = strings.Join(val, " ")
	}

	//check device_model
	if val, ok := mapParams["device_model"]; !ok {
		return paramObj, errors.New("missing param: device_model")
	} else {
		paramObj.device_model = strings.Join(val, " ")
	}

	//check app_name
	if val, ok := mapParams["app_name"]; !ok {
		return paramObj, errors.New("missing param: app_name")
	} else {
		paramObj.app_name = strings.Join(val, " ")
	}

	//check app_ver
	if val, ok := mapParams["app_ver"]; !ok {
		return paramObj, errors.New("missing param: app_ver")
	} else {
		paramObj.app_ver = strings.Join(val, " ")
	}

	//check bundle_id
	if val, ok := mapParams["bundle_id"]; !ok {
		return paramObj, errors.New("missing param: bundle_id")
	} else {
		paramObj.bundle_id = strings.Join(val, " ")
	}

	//check screen_width
	if val, ok := mapParams["screen_width"]; !ok {
		return paramObj, errors.New("missing param: screen_width")
	} else {
		screen_width, err := strconv.ParseInt(strings.Join(val, " "), 10, 0)
		if err != nil {
			return paramObj, errors.New("invalid param: screen_width")
		}
		paramObj.screen_width = int(screen_width)
	}

	//check screen_height
	if val, ok := mapParams["screen_height"]; !ok {
		return paramObj, errors.New("missing param: screen_height")
	} else {
		screen_height, err := strconv.ParseInt(strings.Join(val, " "), 10, 0)
		if err != nil {
			return paramObj, errors.New("invalid param: screen_height")
		}
		paramObj.screen_height = int(screen_height)
	}

	//check sdk_version
	if val, ok := mapParams["sdk_version"]; !ok {
		return paramObj, errors.New("missing param: sdk_version")
	} else {
		paramObj.sdk_version = strings.Join(val, " ")
	}

	//check access_token
	if val, ok := mapParams["access_token"]; !ok {
		return paramObj, errors.New("missing param: access_token")
	} else {
		paramObj.access_token = strings.Join(val, " ")
	}

	//check app_domain
	if val, ok := mapParams["app_domain"]; !ok {
		return paramObj, errors.New("missing param: app_domain")
	} else {
		paramObj.app_domain = strings.Join(val, " ")
	}

	//check adv_type
	if val, ok := mapParams["adv_type"]; !ok {
		return paramObj, errors.New("missing param: adv_type")
	} else {
		paramObj.adv_type = strings.Join(val, " ")
	}

	//check res_type
	if val, ok := mapParams["res_type"]; !ok {
		return paramObj, errors.New("missing param: res_type")
	} else {
		paramObj.res_type = strings.Join(val, " ")
	}

	//check ad_type
	if val, ok := mapParams["ad_type"]; !ok {
		return paramObj, errors.New("missing param: ad_type")
	} else {
		paramObj.ad_type = strings.Join(val, " ")
	}

	//check app_privacy
	if val, ok := mapParams["app_privacy"]; !ok {
		return paramObj, errors.New("missing param: app_privacy")
	} else {
		paramObj.app_privacy = strings.Join(val, " ")
	}

	//check cb
	if val, ok := mapParams["cb"]; !ok {
		return paramObj, errors.New("missing param: cb")
	} else {
		paramObj.cb = strings.Join(val, " ")
	}

	//check more as needed

	return paramObj, nil
}
