package params

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

// TestParamAPI sends http requests to /sample with query params to test our processing of these parameters
func TestParamAPI(t *testing.T) {
	t.Log("\n##### testing api to retrieve query params from url #####\n")

	//create request
	req, err := http.NewRequest("GET", "/sample?pid=PG-2-10143-00000450&ip=24.115.255.255&ua=Mozilla%2F5.0%20%28iPad%3B%20CPU%20OS%208_4%20like%20Mac%20OS%20X%29%20AppleWebKit%2F600.1.4%20%28KHTML%2C%20like%20Gecko%29%20Mobile%2F12H143&device_id=07c20fdf-d97c-4c36-96e1-3a7951167dfb&limit_ad_track=true&ad_format=ALL&lang=en&st=mobile_app&os_type=IOS&portrait=true&device_make=Apple&device_model=iPad3,1&app_name=PGMSDKSample&app_ver=1.0&bundle_id=com.47Billion.monetizeSDKTest&screen_width=768&screen_height=1024&sdk_version=1.2.2&access_token=33323f57-e882-4c1e-a221-f854017ac932&app_domain=mygame.pg.com&adv_type=VIDEO&res_type=VAST&ad_type=VPIMP4&app_privacy=1&cb=234430&did=test&bid=test&lat=37.3830553&long=-122.0056697&country=us&state=ca&city=sunnyvale&dma=san%20francisco-oak-san%20jose&zip=94085", nil)
	if err != nil {
		t.Error("error with request", err)
		return
	}

	//create ResponseRecorder to record response
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(ParamHandler)

	t.Log("sending GET request to URL: /sample?pid=PG-2-10143-00000450&ip=24.115.255.255&ua=Mozilla%2F5.0%20%28iPad%3B%20CPU%20OS%208_4%20like%20Mac%20OS%20X%29%20AppleWebKit%2F600.1.4%20%28KHTML%2C%20like%20Gecko%29%20Mobile%2F12H143&device_id=07c20fdf-d97c-4c36-96e1-3a7951167dfb&limit_ad_track=true&ad_format=ALL&lang=en&st=mobile_app&os_type=IOS&portrait=true&device_make=Apple&device_model=iPad3,1&app_name=PGMSDKSample&app_ver=1.0&bundle_id=com.47Billion.monetizeSDKTest&screen_width=768&screen_height=1024&sdk_version=1.2.2&access_token=33323f57-e882-4c1e-a221-f854017ac932&app_domain=mygame.pg.com&adv_type=VIDEO&res_type=VAST&ad_type=VPIMP4&app_privacy=1&cb=234430&did=test&bid=test&lat=37.3830553&long=-122.0056697&country=us&state=ca&city=sunnyvale&dma=san%20francisco-oak-san%20jose&zip=94085")

	//serve the request
	handler.ServeHTTP(rr, req)

	t.Log("response status code:", rr.Code)

	if rr.Code != 200 {
		t.Error("bad status code: wanted 200, got", rr.Code)
		return
	}

	t.Log("body of response:", rr.Body)

	//test with invalid request
	t.Log("testing with invalid ip address parameter")

	req, err = http.NewRequest("GET", "/sample?pid=PG-2-10143-00000450&ip=24.115.255.255.255&ua=Mozilla%2F5.0%20%28iPad%3B%20CPU%20OS%208_4%20like%20Mac%20OS%20X%29%20AppleWebKit%2F600.1.4%20%28KHTML%2C%20like%20Gecko%29%20Mobile%2F12H143&device_id=07c20fdf-d97c-4c36-96e1-3a7951167dfb&limit_ad_track=true&ad_format=ALL&lang=en&st=mobile_app&os_type=IOS&portrait=true&device_make=Apple&device_model=iPad3,1&app_name=PGMSDKSample&app_ver=1.0&bundle_id=com.47Billion.monetizeSDKTest&screen_width=768&screen_height=1024&sdk_version=1.2.2&access_token=33323f57-e882-4c1e-a221-f854017ac932&app_domain=mygame.pg.com&adv_type=VIDEO&res_type=VAST&ad_type=VPIMP4&app_privacy=1&cb=234430&did=test&bid=test&lat=37.3830553&long=-122.0056697&country=us&state=ca&city=sunnyvale&dma=san%20francisco-oak-san%20jose&zip=94085", nil)
	if err != nil {
		t.Error("error with request", err)
		return
	}

	//create ResponseRecorder to record response
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(ParamHandler)

	//serve the request
	handler.ServeHTTP(rr, req)

	t.Log("response status code:", rr.Code)

	if rr.Code != 404 {
		t.Error("bad status code: wanted 404, got", rr.Code)
		return
	}

	t.Log("body of response:", rr.Body)

	t.Log("Success!")

}
