package db

import (
	//"encoding/json"
	"errors"
	//"fmt"
	"strings"
	"time"

	"github.com/chasex/redis-go-cluster"
)

// The addrs variable holds the addresses for the start nodes of our Redis cluster
var addrs = []string{"127.0.0.1:7001", "127.0.0.1:7002", "127.0.0.1:7003"}

var cluster *redis.Cluster

// init creates a new instance of a Redis cluster
func init() {
	var err error
	cluster, err = redis.NewCluster(
		&redis.Options{
			StartNodes:   addrs,
			ConnTimeout:  50 * time.Millisecond,
			ReadTimeout:  50 * time.Millisecond,
			WriteTimeout: 50 * time.Millisecond,
			KeepAlive:    16,
			AliveTime:    60 * time.Second,
		})
	if err != nil {
		panic("error connecting to cluster")
	}
}

// sIsMember checks if the given value is present in the set given by key
func sIsMember(key, field string) (bool, error) {
	return redis.Bool(cluster.Do("SISMEMBER", key, field))
}

// hgetall queries the cluster for the string parameter key, and returns that whole entry in a map[string]string. In the first return parameter, it returns an error, or nil if successful.
func hGetAll(key string) (map[string]string, error) {
	response, err := redis.StringMap(cluster.Do("HGETALL", strings.ToLower(key)))
	if err != nil {
		return nil, err
	} else if len(response) == 0 {
		return nil, errors.New("no entry found for key " + key)
	}
	return response, nil
}

// hget queries the cluster for the given field in the entry identified by the given key, and returns the value for that field as a string. In the first return parameter, it returns an error, or nil if successful
func hGet(key, field string) (string, error) {
	return redis.String(cluster.Do("HGET", strings.ToLower(key), strings.ToLower(field)))
}

// getSMembers returns an array of all the members of the set specified by key
func getSMembers(key string) ([]string, error) {
	return redis.Strings(cluster.Do("SMEMBERS", key))
}

// SetKeyWithExp adds the value val to key such that it expires and is removed in exp seconds
func SetKeyWithExp(key, val string, exp int) error {
	_, err := cluster.Do("SETEX", key, exp, val)
	return err
}

// IsDeviceBlacklisted checks to see if the given deviceID is on the list of blacklisted devices
func IsDeviceBlacklisted(deviceID string) (bool, error) {
	key := "pg:device:block"
	return sIsMember(key, deviceID)
}

// AdNetworkConstants returns the map of fields to values of the hash specified by pg:const.{network}
func AdNetworkConstants(network string) (map[string]string, error) {
	key := "pg:const." + network
	return hGetAll(key)
}

// AdNetworkConfigs returns the map of fields to values of the hash specified by pg:config.{placement}.{network}
func AdNetworkConfigs(network, placement string) (map[string]string, error) {
	key := "pg:config." + placement + "." + network
	return hGetAll(key)
}

// BidsForPlacement returns the value at key pg:bids:s:{advType}:{partner} and field placement in JSON under the form of a []byte that can be unmarshalled into a struct if needed
func BidsForPlacement(advType, partner, placement string) ([]byte, error) {
	key := "pg:bids:s:" + advType + ":" + partner
	field := placement
	reply, err := hGet(key, field)
	if err != nil {
		return nil, err
	}
	return []byte(reply), nil
}

// GetDeviceIDSegments returns an array of all segments for the given device id
func GetDeviceIDSegments(device string) ([]string, error) {
	key := device
	return getSMembers(key)
}

// GetIPSegments returns an array of all segments for the given ip
func GetIPSegments(ip string) ([]string, error) {
	key := ip
	return getSMembers(key)
}

// CheckBundleID checks if the given bundle id is present in the set given by groupID
func CheckBundleId(groupID, bundleID string) (bool, error) {
	key := "pg:bid." + groupID
	return sIsMember(key, bundleID)
}

// LoadPlacementConfigData returns a map[string]string of the config data for the given placement
func LoadPlacementConfigData(placement string) (map[string]string, error) {
	key := "pg:partner:" + placement
	return hGetAll(key)
}

// GetBlacklistedBundleID returns an array of all blacklisted bundle ids
func GetBlacklistedBundleID() ([]string, error) {
	key := "pg:bundle:block"
	return getSMembers(key)
}

// GetDefaultFcap returns a map[string]string of the default fcap data from the db
func GetDefaultFcap() (map[string]string, error) {
	key := "pg:fcap:default"
	return hGetAll(key)
}

// IsBoosterBundleID returns whether or not the given field (bundle id?) is a member of the key (list of boosters?)
func IsBoosterBundleID(key, field string) (bool, error) {
	return sIsMember(key, field)
}

// GetSmemberList returns the list of all values under the given key
func GetSmemberList(key string) ([]string, error) {
	return getSMembers(key)
}

// CheckPostalCode returns whether or not the given postal code is part of the key specified by the given group id
func CheckPostalCode(groupID, postalCode, cn string) (bool, error) {
	key := "pg:pc:" + cn + "." + groupID
	return sIsMember(key, postalCode)
}

// GetLocodeFromGeoID returns the locode stored under the given geoname id
func GetLocodeFromGeoID(geonameID string) (string, error) {
	key := "pg:geonameid:locode"
	return hGet(key, geonameID)
}

// GetDSPURLs returns the list of all DSP URLs from the db
func GetDSPURLs() ([]string, error) {
	key := "pg:dsp:urls"
	return getSMembers(key)
}
