package db

import (
	"encoding/json"
	// "fmt"
	"reflect"
	"testing"
	"time"

	"github.com/chasex/redis-go-cluster"
)

// loadSimple loads test data for robust test
func loadSimple(t *testing.T) {
	t.Log("##### testing redis cluster... #####")

	//load some sample data for testing
	t.Log("adding 'pid:1, country: us, state: ca' to db")
	_, err := cluster.Do("HMSET", "pid:1", "country", "us", "state", "ca")
	if err != nil {
		t.Error("error putting data in to db", err)
		return
	}

	t.Log("adding 'pid:2, country: us, state: tx' to db")
	_, err = cluster.Do("HMSET", "pid:2", "country", "us", "state", "tx")
	if err != nil {
		t.Error("error putting data in to db", err)
		return
	}
}

// TestErrors makes sure we get an error when we try to get something that doesn't exist yet
func TestErrors(t *testing.T) {
	t.Log("trying to get something that doesn't exist")
	_, err := hGetAll("pid:4")
	if err == nil {
		t.Error("did not get an error for invalid key")
		return
	}

	_, err = hGet("pid:2", "zip")
	if err == nil {
		t.Error("did not get an error for invalid field")
		return
	}
}

// TestSimple does simple tests on a small data set for hGet and hGetAll functions
func TestSimple(t *testing.T) {
	loadSimple(t)
	//TEST hGetAll
	t.Log("getting pid:1 from db")

	response, err := hGetAll("pid:1")
	if err != nil {
		t.Error("error fetching data form db:", err)
		return
	}
	t.Log("received", response)

	expected := map[string]string{"country": "us", "state": "ca"}
	if !reflect.DeepEqual(expected, response) {
		t.Error("received incorrect response, wanted", expected, "but got", response)
		return
	}

	//TEST hGet
	t.Log("getting state for pid:2 from db")

	resp, err := hGet("pid:2", "state")
	if err != nil {
		t.Error("error fetching data from db:", err)
		return
	}
	t.Log("received " + resp)

	if "tx" != resp {
		t.Error("received incorrect response, wanted 'tx' but got", resp)
		return
	}

	//clear sample data from database
	t.Log("clearing data from db")
	cluster.Do("FLUSHDB")
}

// loadRobust loads test data for robust test
func loadRobust(t *testing.T) {
	//TEST parts again with more robust test entry
	t.Log("adding 'pid:12345-abc-4345-sasd, country: us, state: ca, lat: -67.2345723, portrait: true, ua: Mozilla/5.0 (iPad; CPU OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12H143, device_id: 07c20fdf-d97c-4c36-96e1-3a7951167dfb' to db")
	_, err := cluster.Do("HMSET", "pid:12345-abc-4345-sasd", "country", "us", "state", "ca", "lat", "-67.2345723", "portrait", "true", "ua", "Mozilla/5.0 (iPad; CPU OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12H143", "device_id", "07c20fdf-d97c-4c36-96e1-3a7951167dfb")
	if err != nil {
		t.Error("error putting data in to db", err)
		return
	}
}

// TestRobust does more robust test on a larger data set for hGet and hGetAll functions
func TestRobust(t *testing.T) {
	loadRobust(t)
	//TEST hGetAll
	t.Log("getting pid:12345-abc-4345-sasd from db")

	response, err := hGetAll("pid:12345-abc-4345-sasd")
	if err != nil {
		t.Error("error fetching data from db:", err)
		return
	}
	t.Log("received", response)

	expected := map[string]string{"country": "us", "state": "ca", "lat": "-67.2345723", "portrait": "true", "ua": "Mozilla/5.0 (iPad; CPU OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12H143", "device_id": "07c20fdf-d97c-4c36-96e1-3a7951167dfb"}
	if !reflect.DeepEqual(expected, response) {
		t.Error("received incorrect response, wanted", expected, "but got", response)
		return
	}

	//TEST hGet
	t.Log("getting lat for pid:12345-abc-4345-sasd from db")

	resp, err := hGet("pid:12345-abc-4345-sasd", "lat")
	if err != nil {
		t.Error("error fetching data from db:", err)
		return
	}
	t.Log("received " + resp)

	if "-67.2345723" != resp {
		t.Error("received incorrect response")
	}

	//clear sample data from database
	t.Log("clearing data from db")
	cluster.Do("FLUSHDB")
}

// TestSetex provides a basic test for the SetKeyWithExp function
func TestSetex(t *testing.T) {
	//TEST SetKeyWithExp
	err := SetKeyWithExp("pid:3", "val", 2)
	if err != nil {
		t.Error("error setting with exp:", err)
		return
	}
	time.Sleep(time.Second)
	ttl, err := redis.Int(cluster.Do("TTL", "pid:3"))
	if err != nil || ttl != 1 {
		t.Error("problem with TTL")
		return
	}
	time.Sleep(time.Second)
	_, err = redis.String(cluster.Do("GET", "pid:3"))
	if err != redis.ErrNil {
		t.Error("did not expire")
		return
	}

}

// TestBlacklist tests IsDeviceBlacklisted with deviceIDs that do and do not exist in set and uses the same loaded set to check getSMembers
func TestBlackListAndGetSMembers(t *testing.T) {
	_, err := cluster.Do("SADD", "pg:device:block", "1", "2", "4", "5")
	if err != nil {
		t.Error("error with SADD", err)
		return
	}
	// test ID that doesn't exist
	exists, err := IsDeviceBlacklisted("3")
	if err != nil {
		t.Error("error with IsDeviceBlacklisted", err)
		return
	}
	if exists {
		t.Error("found deviceID when it wasn't on blacklist")
		return
	}
	//test ID that does exist
	exists, err = IsDeviceBlacklisted("4")
	if err != nil {
		t.Error("error with IsDeviceBlacklisted", err)
		return
	}
	if !exists {
		t.Error("didn't find deviceID when it was on blacklist")
		return
	}

	expected := []string{"1", "2", "4", "5"}
	mem, err := getSMembers("pg:device:block")
	if err != nil {
		t.Error("error with getSMembers", err)
		return
	}
	if !reflect.DeepEqual(expected, mem) {
		t.Error("getSMembers failed to return correct set. exp:", expected, "got:", mem)
		return
	}

	cluster.Do("FLUSHDB")
}

// TestBidsForPlacement tests the BidsForPlacement function
func TestBidsForPlacement(t *testing.T) {
	_, err := cluster.Do("HSET", "pg:bids:s:advtype:partner", "placement", `{"message": "test", "code": "418"}`)
	if err != nil {
		t.Error("error with HSET", err)
		return
	}
	_, err = BidsForPlacement("advtype", "partner", "placement")
	if err != nil {
		t.Error("error with BidsForPlacement:", err)
		return
	}

	// NOTE: struct names must be uppercase (exported) otherwise this will not work
	type BFP struct {
		Message string
		Code    string
	}
	bfp := BFP{}
	str := `{"message": "test", "code": "418"}`

	err = json.Unmarshal([]byte(str), &bfp)
	if err != nil {
		t.Error("error with unmarshaling:", err)
		return
	}

	if bfp.Message != "test" || bfp.Code != "418" {
		t.Error("didn't get right values from unmarshal")
		return
	}
	cluster.Do("FLUSHDB")
}
